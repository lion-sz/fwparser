#include "../patft.h"

PyObject *set_omitted(PyObject *self, PyObject *args) {

    if (!PyArg_ParseTuple(args, "O!", &PyList_Type, &omit_list)) {
        PyErr_SetString(PyExc_TypeError, "parameter must be a list.");
        return NULL;
    }

    omit_tags_num = PyList_Size(omit_list);
    PyObject *item = NULL;
    omit_tags = calloc(omit_tags_num, sizeof(char *));

    for (size_t i = 0; i < omit_tags_num; i++) {
        item = PyList_GetItem(omit_list, i);
        if (!PyUnicode_Check(item)) {
            PyErr_SetString(PyExc_TypeError, "Argument must be List strings!");
            return NULL;
        }
        PyObject *tmp = PyUnicode_AsEncodedString(item, "utf-8", "strict");
        if (tmp == NULL) {
            return NULL;
        }
        omit_tags[i] = (char *) calloc(PyBytes_Size(tmp) + 1, 1);
        memcpy(omit_tags[i], PyBytes_AsString(tmp), PyBytes_Size(tmp) + 1);
        Py_DECREF(tmp);
    }

    return Py_BuildValue("");
}
