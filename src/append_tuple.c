#include "../patft.h"

int append_tuple(PyObject *list, char *key_str, char *val_str) {

    for (size_t i = 0; i < omit_tags_num; i++) {
        if (strcmp(key_str, omit_tags[i]) == 0) {
            return 0;
        }
    }

    PyObject *key = Py_BuildValue("s", key_str);
    if (key == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "Key string is NULL");
        return -1;
    }

    PyObject *val = NULL;
    if (*val_str == 0) {
        val = Py_BuildValue("");
    } else {
        val = Py_BuildValue("s", "hey");
        PyObject *tmp = PyBytes_FromFormat("%s", val_str);
        if (tmp == NULL) {
            return -1;
        }
        val = PyUnicode_FromEncodedObject(tmp, "ascii", "strict");
        val = PyUnicode_Decode(val_str, strlen(val_str), "utf-8", "strict");
        if (val == NULL) {
            return -1;
        }
    }

    PyObject *tuple = PyTuple_Pack(2, key, val);
    if (tuple == NULL) {
        return -1;
    }
    PyList_Append(list, tuple);
    Py_DECREF(key);
    Py_DECREF(val);
    Py_DECREF(tuple);
    return 0;
}
