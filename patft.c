#include "patft.h"


const char doc_bound[30] = "*** BRS DOCUMENT BOUNDARY  ***";
const char white[5] = "    ";
const char *filepath = NULL;
FILE *file = NULL;
bool file_readable = false;

PyObject *omit_list = NULL;
char **omit_tags = NULL;
size_t omit_tags_num = 0;

static PyMethodDef patftmethods[] = {
    {"open",  open_patft, METH_VARARGS,
     "Open a new file for parsing."},
    {"get", get_entry, METH_VARARGS,
     "Returns an single entry"},
    {"set_omitted", set_omitted, METH_VARARGS,
     "Set a list of tags that are skipped in the parsing."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef patftmodule = {
    PyModuleDef_HEAD_INIT,
    "patft",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    patftmethods
};

PyMODINIT_FUNC
PyInit_patft(void)
{
    return PyModule_Create(&patftmodule);
}
