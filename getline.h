
#ifndef __unix__

    #include <nbcompat.h>
    #include <nbcompat/stdio.h>
    #include <nbcompat/stdlib.h>

    #if !HAVE_GETDELIM
    ssize_t getdelim(char **, size_t *, int, FILE *);
    #endif

    #if !HAVE_GETLINE
    ssize_t getline(char **, size_t *, FILE *);
    #endif
#endif