#include "../patft.h"

PyObject *get_entry(PyObject *self, PyObject *args) {

    if (!file_readable) {
        return Py_BuildValue("");
    }

    if (file == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "File not initialized.");
        return NULL;
    }

    PyObject *entry = PyList_New(0);
    if (entry == NULL) {
        return NULL;
    }

    char *cont = NULL;
    size_t cont_len = 0;
    size_t read;

    char *key_str = calloc(5, sizeof(char));
    size_t val_str_len = 256;
    char *val_str = calloc(val_str_len, sizeof(char));

    // Iterate through lines. I remove tailing whitespaces.
    while (true) {
        // On EOF the result of getline is -1
        if (getline(&cont, &cont_len, file) == -1) {
            append_tuple(entry, key_str, val_str);
            file_readable = false;
            break;
        };
        cont[strcspn(cont, "\n")] = 0;
        read = strlen(cont);

        // Append this line to the buffer and continue.
        if (strncmp(cont, white, 4) == 0) {
            if (strlen(val_str) + read + 1 >= val_str_len) {
                while (strlen(val_str) + read + 1 >= val_str_len) {
                    val_str_len = val_str_len * 2;
                }
                char *tmp = calloc(val_str_len, 1);
                strcpy(val_str, tmp);
                free(val_str);
                val_str = tmp;
            }
            val_str[strlen(val_str)] = ' ';
            strcpy(val_str + strlen(val_str), cont + 5);
            continue;
        }

        // Write the old line (except in the first iteration) and reset the buffer.
        if (*key_str != 0) {
            if (append_tuple(entry, key_str, val_str) == -1)
                return NULL;
        }
        memset(val_str, 0, val_str_len);

        // Stop the iteration when encountering a doc boundary
        if (strncmp(cont, doc_bound, 25) == 0) {
            break;
        }

        // Set the key and value.
        memcpy(key_str, cont, 4);
        if (*(key_str + 3) == ' ') {
            *(key_str + 3) = 0;
        }
        // If there are at most 6 Bytes read, this is a key without content.
        // In this case I leave val_str blank, as the append tuple function
        // can deal with this.
        if (read > 6) {
            if (read >= val_str_len) {
                PyErr_SetString(PyExc_RuntimeError, "2");
                return NULL;
                while (read >= val_str_len) {
                    printf("Realloc");
                    val_str_len = val_str_len * 2;
                }
                val_str = (char *) realloc(val_str, val_str_len * sizeof(char));
            }
            strcpy(val_str, cont + 5);
        }

    }

    free(key_str);
    if (cont != NULL) {
        free(cont);
    }

    return entry;
}
