#include "../patft.h"


PyObject *open_patft(PyObject *self, PyObject *args) {

    if (!PyArg_ParseTuple(args, "s", &filepath)) {
        PyErr_SetString(PyExc_RuntimeError, "Reading arguments failed, arguments not string?");
        return NULL;
    }

    file = fopen(filepath, "r");
    if (file == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "File opening failed, missing file?");
        return NULL;
    }

    // I need to remove the first line of the file.
    char *cont = NULL;
    size_t len;
    if (getline(&cont, &len, file) == -1) {
        PyErr_SetString(PyExc_RuntimeError, "Empty file found!.");
        return NULL;
    }
    file_readable = true;
    return Py_BuildValue("");
};

