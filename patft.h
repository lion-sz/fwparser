#ifndef PATFT
#define PATFT

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "getline.h"

PyObject *open_patft(PyObject *, PyObject *);
PyObject *set_omitted(PyObject *, PyObject *);
PyObject *get_entry(PyObject *, PyObject *);
PyObject *get_entry2(PyObject *, PyObject *);
int append_tuple(PyObject *list, char *key_str, char *val_str);


extern const char doc_bound[];
extern const char white[];
extern const char *filepath;
extern FILE *file;
extern bool file_readable;

extern PyObject *omit_list;
extern char **omit_tags;
extern size_t omit_tags_num;

#endif