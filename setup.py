from distutils.core import setup, Extension

#module1 = Extension('patft', ['patft.c', 'src.c'])
module1 = Extension('patft', ['patft.c', 'src/getline.c',
                              'src/open_patft.c', 'src/set_omitted.c',
                              'src/append_tuple.c', 'src/get_entry.c'
                              ], ['-Wall'])

setup (name = 'patft',
       version = '1.0',
       description = 'Reading in PATFT files.',
       ext_modules = [module1])