import patft
import itertools

omitted_keys = [
    'DEPR', 'CLPR', 'BSPR', 'CLNO', 'DRPR', 'CIFS', 'ABPR'
]

#print(patft.open('us4.txt'))
print(patft.open('/home/lion/tmp/patft/us2.txt'))
print(patft.set_omitted(omitted_keys))

key_collections = []
num = 0
for i in range(100000):
    entry = patft.get()

    if entry is None:
        num = i
        break

    key_collections.append([key for key, value in entry])


keys = list(itertools.chain.from_iterable(key_collections))
counts = {}
for key in set(keys):
    counts[key] = keys.count(key) / num

print(dict(sorted(counts.items(), key = lambda x: x[1], reverse=True)))
